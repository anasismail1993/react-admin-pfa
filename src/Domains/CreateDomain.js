import * as React from "react";
import { Create,ImageInput,ImageField, Edit, SimpleForm, TextInput, DateInput, ReferenceManyField, Datagrid, TextField, DateField, EditButton } from 'react-admin';
import RichTextInput from 'ra-input-rich-text';

export const CreateDomain = (props) => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="title" />
            <TextInput source="Description" options={{ multiLine: true }} />
            <ImageInput source="Image" label="image" accept="image/*" multiple={false}>
                <ImageField source="src" label="Domain" />
            </ImageInput>
            <DateInput label="Publication date" source="published_at" defaultValue={new Date()} />
        </SimpleForm>
    </Create>
);

export default CreateDomain;