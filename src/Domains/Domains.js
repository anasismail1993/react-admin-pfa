import * as React from "react";
import {
    Datagrid,
    List,
    Show,
    Create,
    Edit,
    ImageField,
    Filter,
    SimpleShowLayout,
    SimpleForm,
    TextField,
    TextInput,
    ShowButton,
    EditButton,
    DeleteButton,
  } from "react-admin";



const DomainsList = (props) => (
    <List {...props} >
    <Datagrid>
      <TextField source="title" />
      <TextField source="Description" />
      <ImageField source="Image.src" title="title" />
      <ShowButton label="" />
      <EditButton label="" />
      <DeleteButton label="" redirect={false}/>
    </Datagrid>
  </List>
);

export default DomainsList;