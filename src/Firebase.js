import * as React from 'react';
import { Admin, Resource } from 'react-admin';


import {
  FirebaseAuthProvider,
  FirebaseDataProvider,
  FirebaseRealTimeSaga
} from 'react-admin-firebase';

const config = {
    apiKey: "AIzaSyBtXQhICV4xOzzxI3vWhrhFO7cxjQwZ4_c",
    authDomain: "proadvice-6ac32.firebaseapp.com",
    databaseURL: "https://proadvice-6ac32.firebaseio.com",
    projectId: "proadvice-6ac32",
    storageBucket: "proadvice-6ac32.appspot.com",
    messagingSenderId: "987255700485",
    appId: "1:987255700485:web:44694b7cbb9efda305b06c",
    measurementId: "G-BL2RJ6BDF5"
};

const options = {
  
};

const  dataProvider = FirebaseDataProvider(config, options);

// const firebaseRealtime = FirebaseRealTimeSaga(dataProvider, options);
export default dataProvider;
