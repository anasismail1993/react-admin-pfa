import * as React from "react";
import { Create, Edit,ImageInput,ImageField, SimpleForm,SelectInput, TextInput,ReferenceInput, DateInput, ReferenceManyField, Datagrid, TextField, DateField, EditButton } from 'react-admin';
import RichTextInput from 'ra-input-rich-text';
import dataProvider from "../Firebase";



export const CreateCompanie = (props) => (

    <Create {...props}>
        <SimpleForm>
            <TextInput source="title" />
            <TextInput source="Adress" />
            <ReferenceInput label="Domain" source="Domains" reference="Domains">
    <SelectInput optionText="title"   options={{ fullWidth: true }} />
</ReferenceInput>
<ImageInput source="Image" label="image" accept="image/*" multiple={false}>
                <ImageField source="src" label="Companies" />
            </ImageInput>
            <TextInput source="Description" options={{ multiLine: true }} />
            <DateInput label="Publication date" source="published_at" defaultValue={new Date()} />
        </SimpleForm>
        </Create>

);


export default CreateCompanie;