import * as React from "react";
import {
    Datagrid,
    List,
    Show,
    Create,
    Edit,
    Filter,
    SimpleShowLayout,
    SimpleForm,
    TextField,
    TextInput,
    ShowButton,
    ImageField,
    ReferenceField,
    EditButton,
    DeleteButton,
  } from "react-admin";



const CompaniesList = (props) => (
    <List {...props} >
    <Datagrid>
      <TextField source="title" />
      <TextField source="Adress" />
      <ReferenceField label="Domain" source="Domains" reference="Domains">
                <TextField source="title" />
            </ReferenceField>
      <TextField source="Description" />
      <ImageField source="Image.src" title="title" />
      <ShowButton label="" />
      <EditButton label="" />
      <DeleteButton label="" redirect={false}/>
    </Datagrid>
  </List>
);

export default CompaniesList;