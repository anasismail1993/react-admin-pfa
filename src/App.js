import * as React from "react";

import { Admin, Resource } from 'react-admin';
import simpleRestProvider from 'ra-data-simple-rest';
import dataProvider from './Firebase'

import UserList  from './users/users';
import CompaniesList  from './Companies/Companies';
import CreateCompanie  from './Companies/CreateCompanie';
import DomainsList from './Domains/Domains';
import CreateDomain from './Domains/CreateDomain';
import CreateUniversity from './University/CreateUniversity';
import UniversityList from './University/University';
import login from './Login';

const App = () => (
    <Admin dataProvider={dataProvider} loginPage={login}>
        <Resource name="users" options={{ label: 'characters' }} list={UserList} />
        <Resource name="University" options={{ label: 'university' }} create={CreateUniversity} list={UniversityList} />
        <Resource name="Companies" create={CreateCompanie} list={CompaniesList} />
        <Resource name="Domains"  create={CreateDomain} list={DomainsList} />      
    </Admin>
);

export default App;