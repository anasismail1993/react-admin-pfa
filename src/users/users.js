import * as React from "react";
import {
    Datagrid,
    List,
    Show,
    Create,
    Edit,
    Filter,
    SimpleShowLayout,
    SimpleForm,
    TextField,
    useGetOne,
    ShowButton,
    EditButton,
    ReferenceField,
    DeleteButton,
  } from "react-admin";


const users = (props) => (
  
    <List {...props} >
    <Datagrid>
      <TextField source="fullName" />
      <TextField source="mail" />
      <TextField source="role" />
      <ReferenceField label="Company" source="company" reference="Companies">
                <TextField source="title" />
            </ReferenceField>
      <ShowButton label="" />
      <EditButton label="" />
      <DeleteButton label="" redirect={false}/>
    </Datagrid>
  </List>
);

export default users;